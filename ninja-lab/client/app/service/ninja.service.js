class NinjaService {

    /*@ngInject*/
    constructor($http, $q){
        this.$http = $http;
        this.$q = $q;
    }

    getData() {
        return this.$http.get('./app/assets/ninja-list.json').then(res => res.data, err => this.$q.reject(err.error));
    }
}

export default angular.module('app.ninjaService', [])
    .service('NinjaService', NinjaService).name;









//
//
//
// let production = false;
// let devMode = true;
// let domain = 'http://oskcrm.local';
//
// let suffix = '/api';
//
// if(!production && devMode) {
//     suffix = '/app_dev.php/api';
// }
//
// if(!production && !devMode) {
//     suffix = '/app.php/api';
// }
//
// let url = domain + suffix;
//
// export function apiUrl() {
//     'ngInject';
//     return {
//         API_URL: url;
//     };
// }
//
//
//
// import {
//     apiUrl
// } from '../../app.api.config';
//
// export default class EquipmentService {
//     /*@ngInject*/
//     constructor($http) {
//         this.$http = $http;
//     }
//     getList = () => this.$http.get(apiUrl().API_URL + '/equipments/?XDEBUG_SESSION_START=PHPSTORM');
//     save = request => this.$http.post(apiUrl().API_URL + '/equipments?XDEBUG_SESSION_START=PHPSTORM', request);
// }
