import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './weapon.html';

class WeaponController {
  constructor() {
      const vm = this;
      vm.componentName = "Weapons";
      vm.diableOptions = {
          disableContent: {
              name:true
              }
      }
  }
}

export default angular.module('weapon', [uiRouter])

  .component('weapon',{
    template,
    controller: WeaponController,
    controllerAs: 'vm',
    bindings: {},
  })

  // .config(($stateProvider) => {
  //   "ngInject";
  //   $stateProvider
  //     .state('ninja', {
  //       url: '/ninja',
  //       component: 'ninja'
  //     });
  // })

  .name;
