import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './ninja.html';
import NinjaService from  '../../service/ninja.service';


class NinjaController {
    ninjas = [];
    /*@ngInject*/
    constructor(NinjaService) {
        const vm = this;
        vm.componentName ='Ninjas';

        NinjaService.getData().then((data) => {
            vm.ninjas = data.ninjas;
        }, err => {
        });
    }

    onSave(data) {
        this.ninjas.push(data);
    }

    $onChanges(bindings) {
        console.log(bindings);
    };


}

export default angular.module('app.ninja', [uiRouter, NinjaService])

  .component('ninja',{
    template,
    controller: NinjaController,
    controllerAs: 'vm',
    bindings: {
        options: '<'
    },
  })

  .name;








//to przeniesiono do app.js jako glownego pliku stanow
// .config(($stateProvider) => {
//   "ngInject";
//   $stateProvider
//     .state('ninja', {
//       url: '/ninja',
//       component: 'ninja'
//     });
// })
