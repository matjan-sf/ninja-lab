import angular from 'angular';
import Home from './home/home';
import About from './about/about';
import Ninja from './ninja/ninja.component';
import Weapon from './weapon/weapon.component';

let componentModule = angular.module('app.components', [
  Home,
  About,
  Ninja,
  Weapon
])

.name;

export default componentModule;
