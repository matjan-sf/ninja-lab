import angular from 'angular';
import template from './ninja-add.html';


class NinjaAddController {
    /*@ngInject*/
    constructor() {
    const vm = this;
    vm.ninjaForm = {};
    vm.ninjaFormData = {
        name: null,
        weapon: null
    };

    vm.addNinja = () => {
        if (vm.ninjaForm.$valid) {
            this.save({data: vm.ninjaFormData});
            this.clearFormFields();
        }
    };
  }

  clearFormFields() {
      this.ninjaFormData = {
          name: null,
          weapon: null
      };
  }

    $onChanges(bindings) {
        console.log(bindings);
    };
}

export default angular.module('ninjaAdd', [])

  .component('ninjaAdd',{
    template,
    controller: NinjaAddController,
    controllerAs: 'vm',
    bindings: {
        save: '&',
        disabler: '<'
    },
  })
  .name;
