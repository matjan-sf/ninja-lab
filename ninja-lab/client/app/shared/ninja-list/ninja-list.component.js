import angular from 'angular';
import template from './ninja-list.html';



class NinjaListController {
    /*@ngInject*/
    constructor() {
    }

    onSaveSuperData(ninjaData) {
        this.save({data: ninjaData});
    }

    $onChanges(bindings) {
        console.log(bindings);
    };
}



export default angular.module('ninjaList', [])
  .component('ninjaList',{
    template,
    controller: NinjaListController,
    controllerAs: 'vm',
    bindings: {
        ninjaData: "<",
        save: '&',
        option: '<'
    },
  })
  .name;
