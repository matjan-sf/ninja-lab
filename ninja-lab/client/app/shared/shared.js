import angular from 'angular';
import NinjaAdd from './ninja-add/ninja-add.component';
import NinjaList from './ninja-list/ninja-list.component';


let sharedModule = angular.module('app.shared', [
    NinjaAdd,
    NinjaList

])
    .name;

export default sharedModule;
