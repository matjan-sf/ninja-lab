import angular from 'angular';
import './navbar.scss';
import template from './navbar.html'
import uiRouter from 'angular-ui-router';


class NavbarController {
  constructor() {
    this.name = 'navbar';
  }
}



export default angular.module('navbar', [uiRouter])
  .component('navbar', {
    template,
    controller: NavbarController,
    bindings: {},
  })
  .name;
