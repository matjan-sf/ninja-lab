import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'normalize.css';
import NinjaService from './service/ninja.service';
import Shared from './shared/shared'

angular.module('app', [
    uiRouter,
    Common,
    Components,
    Shared,
    NinjaService
  ])

  .config(($stateProvider) => {
    "ngInject";
    $stateProvider
      .state('ninja', {
        url: '/ninja',
        component: 'ninja'
      })
      .state('about', {
        url: '/about',
        component: 'about'
      })
        .state('weapon', {
            url: '/weapon',
            component: 'weapon'
        })

  })





  // .config(($locationProvider) => {
  //   "ngInject";
  //   // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
  //   // #how-to-configure-your-server-to-work-with-html5mode
  //   $locationProvider.html5Mode(true).hashPrefix('!');
  // })

  .component('app', AppComponent);
